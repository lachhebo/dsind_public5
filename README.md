# Formation Industrialisation d'un projet de data science

This is a practical work to illustrate Data Science industrialization.

- [Formation Industrialisation d'un projet de data science](#formation-industrialisation-dun-projet-de-data-science)
  - [How do I install it](#how-do-i-install-it)
  - [How to follow it](#how-to-follow-it)
  - [For windows users](#for-windows-users)
  - [For linux users](#for-linux-users)

## How do I install it

First, make sure you have miniconda or anaconda installed. If not, install it!

Create a conda env

```sh
conda create -n python_indus python=3.8
conda activate python_indus
```

Retrieve project from gitlab

```sh
git clone git@gitlab.com:etoulemonde/formation_indus_ds.git
```

Start a jupyter notebook in the folder

```sh
cd formation_indus_ds
jupyter-notebook
```

If it's not working, make your jupyter-notebook is installed. To install it :

```sh
pip install jupyter
```

If your `python_indus` environment is not available in `jupyter` interface (when clicking on new). You should :

- Quit jupyter-notebook with <kbd>ctrl</kbd>+<kbd>c</kbd> in terminal
- Run `conda install -n python_indus nb_conda_kernels`
- Start `jupyter-notebook`

## How to follow it

It is highly linked to the presentation of the formation.

To navigate between steps change branch.

To see all branches

```sh
git branch -a
```

To start the practical work you should checkout branch `0_initial_state`

```sh
git checkout 0_initial_state
```

## For windows users

You will need a `git bash` terminal and a conda terminal :

- All `git` commands should be executed in the `git bash` terminal.
- All `python` and `conda` related commands should be executed in the conda terminal.

## For linux users

Every command can be executed in your terminal.
